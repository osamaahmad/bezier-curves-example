from Curve import Curve
from Math import binomial_coefficient

DEFAULT_CONTROL_POINTS = [(0, 0), (0, 0), (0, 0)]


class BezierCurve(Curve):

    def __init__(self, control_points=None):
        if control_points is None:
            control_points = DEFAULT_CONTROL_POINTS

        self.control_points = control_points

    def get_order(self):
        return len(self.control_points) - 1

    def B(self, n, k, t):
        result = binomial_coefficient(n, k)
        result *= (1 - t) ** k
        result *= t ** (n - k)
        return result

    def B_prime(self, n, k, t):
        one_minus_t_pow = k
        t_pow = n - k

        result = binomial_coefficient(n, k)
        result *= (1 - t) ** max(0, one_minus_t_pow - 1)
        result *= t ** max(0, t_pow - 1)
        result *= (1 - t) * t_pow - t * one_minus_t_pow

        return result

    def multiply_point(self, point, multiplier):
        x = point[0] * multiplier
        y = point[1] * multiplier
        return x, y

    def calculate_point_multiplier(self, t, multiplier_func):
        res_x = 0
        res_y = 0

        n = self.get_order()
        for k in range(n + 1):
            point = self.control_points[k]
            multiplier = multiplier_func(n, k, t)
            point = self.multiply_point(point, multiplier)
            res_x += point[0]
            res_y += point[1]

        return res_x, res_y

    def calculate_point_at(self, t):
        return self.calculate_point_multiplier(t, self.B)

    def calculate_derivative_at(self, t):
        return self.calculate_point_multiplier(t, self.B_prime)

    def calculate_slope_at(self, t):
        p1 = self.calculate_point_at(t)
        p2 = self.calculate_derivative_at(t)

        x1 = p1[0]
        y1 = p1[1]
        x2 = p2[0]
        y2 = p2[1]

        if y1 == y2:
            return 1000000000  # TODO change this

        return (x2 - x1) / (y2 - y1)

    def set_control_point(self, i, point):
        self.control_points[i] = point
